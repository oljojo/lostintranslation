# Lost in translation

This is a translation app for American sign-language. It has translation history and profile features alongside the core translation.

## Install
In order for the app to be executable, it was necessary to install the following npm pakages

```
git clone https://gitlab.com/oljojo/lostintranslation.git
cd lostintranslation
npm install

npm install 
npm install react-hook-form
npm install react-router-dom
```

## Usage

```
npm start
create .env file in "lostintranslation" folder
the file should contain:
    REACT_APP_API_URL=<your api-url>
    REACT_APP_API_KEY=<your api-key>

```

* Opens the App in the browser at (http://localhost:3000/).
* First textbox is for login or create user, just enter a username.
* When logged in you are able to translate text into American sign-language, only letters.
* Profile page is accessed in top right corner and shows the previous translations done.
* Translation history may be cleared and you can signout from the profile page.

## Contributors 

[Olof Johnsson](@oljojo)[Samuel Kesete](@Samuel Kesete)


