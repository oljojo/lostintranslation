const apiUrl = process.env.REACT_APP_API_URL
const apiKey = process.env.REACT_APP_API_KEY

const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    } 
}

const checkUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not fetch request.')
        }
        const data = await response.json();
        console.log(`data: ${data}`)
        return [null, data]
        
    } catch (error) {
        return [error.message, null]
    }

}

const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        });
        if(!response.ok){
            throw new Error ("Could not create a user " + username)

        }
        const data = await response.json()
        return[null, data]
    } catch (error) {
        return [error,[]]

    }

}

export const loginUser = async (username) => {
    const [checkError, user] = await checkUser(username)
    if(checkError !== null) {
        return [checkError, null]
    }
    console.log(`UserLength: ${user.length}`)
    if(user.length > 0){
        return [null, user.pop()]
    }
    console.log(`Creating user`)
    return await createUser(username)
    

}

export const addApiHistory = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        });
        if(!response.ok){
            throw new Error ("Could not add translation")

        }
        const result = await response.json()
        return  [null, result]
    } catch (error) {
        return [error.message, null]
    }
}

export const clearApiHistory = async (user) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        });
        if(!response.ok){
            throw new Error ("Could not clear history")

        }
        const result = await response.json()
        return  [null, result]
    } catch (error) {
        return [error.message, null]
    }
}
