import TranslationForm from "../Components/Translation/TranslationForm";
import TranslationResult from "../Components/Translation/TranslationResult";
import ProfileButton from "../Components/Navbar/ProfileButton";
import withAuth from "../hoc/withAuth";

const Translation = () => {
  return (
    <>
      <div id="translation_con">
      <ProfileButton />
      <div id="Translation">
        
        <TranslationForm />
        <TranslationResult />
      </div>
      </div>
    </>
  );
};
export default withAuth(Translation);
