import withAuth from "../hoc/withAuth"
import ProfileHeader from "../Components/Profile/ProfileHeader"
import TranslationHistory from "../Components/Profile/TranslationHistory"


const Profile = () => {
    return (
        <>
        <div id="profileContainer">
            <div id="profileTop">
                <ProfileHeader />
                <p id="lostInTransText">Lost in Translation</p>
                <p id="loginText">Profile</p>
            </div>
            <div id="profileBottom">
                <TranslationHistory/>
            </div>
        </div>
        </>
    )

}

export default withAuth(Profile)