import LoginForm from "../Components/Login/LoginForm";

const Login = () => {
  return (
    <div id="loginContainer">
      <div id="loginTop">
        <p id="lostInTransText">Lost in Translation</p>
        <p id="loginText">Login</p>
        <img id ="backgroundimage" src="logo.png" alt="image" />
      </div>
      <div id="loginBottom">
        <div id="loginForm">
          <LoginForm />
        </div>
      </div>
    </div>
  );
};

export default Login;
