//save to storage
export const saveStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value))
}
//read from storage
export const readStorage = key => {
    const data = localStorage.getItem(key)
    if (data){
        return JSON.parse(data)
    }
    return null
} 
//delete from storages
export const storageDelete = (key) => {
    localStorage.removeItem(key)
}

export const updateStorageHistory = (key, translation) => {
    const data = JSON.parse(localStorage.getItem(key))
    console.log("Data" + data)
    data.translations.push(translation)
    localStorage.setItem(key, JSON.stringify(data))
    return data

}

export const removeStorageHistory = (key) => {
    const data = JSON.parse(localStorage.getItem(key))
    console.log("Data" + data)
    data.translations = []
    localStorage.setItem(key, JSON.stringify(data))
    return data
}