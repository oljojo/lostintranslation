
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route

} from 'react-router-dom'
import Login from './View/Login';
import Translation from './View/Translation';
import Profile from './View/Profile';

// first commit by me
function App() {
  return (
    <BrowserRouter>

      <div className="App">

        <Routes>
          <Route path="/" element={ <Login /> }/>
          <Route path="/translation" element={ <Translation /> }/>
          <Route path="/profile" element={ <Profile /> }/>
        </Routes>


      </div>
    </BrowserRouter>
  );
}

export default App;
