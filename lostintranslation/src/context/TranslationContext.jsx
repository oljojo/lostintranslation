import { useContext } from "react";
import { createContext, useState } from "react";

const TranslationContext = createContext()


export const useTranslation = () => {
    return useContext(TranslationContext)
}

const TranslationProvider = ({children}) => {

    const [ translation, setTransation ] = useState(null)
    
    const state = {
        translation, setTransation
    }

    return (
        <TranslationContext.Provider value={state}>
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider