
import { createContext, useContext, useState } from "react";

import { readStorage } from "../Utils/Storage";
import {STORAGE_KEY_USER} from "../Const/storageKey"
// context object



const UserContext = createContext()
export const useUser = () => {
    return useContext(UserContext)
}

// provider -> managing state 

const UserProvider = ({ children }) => {
    // magic strings 
    const [user, setUser] = useState(readStorage(STORAGE_KEY_USER))

    const state = {
        user,
        setUser
    }
    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider