import TranslationProvider from "./TranslationContext"
import UserProvider from "./UserContext"

const AppContext = (props) => {

    
    return (
        <UserProvider>
            <TranslationProvider>
                {props.children} {/*same as using ({ children }) in AppContext as argument*/}
            </TranslationProvider>
        </UserProvider>
    )
}

export default AppContext