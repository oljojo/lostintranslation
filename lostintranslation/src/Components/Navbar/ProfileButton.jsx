import { useNavigate } from "react-router-dom"

const ProfileButton = () => {

    const navigate = useNavigate()

    const handleOnClick = () => {
        navigate('../profile')
    }

    return (
        <>
            <div id="profileimage">
                <img src="Profileimg/profile.png" alt="profileImage" onClick={handleOnClick} />
            </div>
        </>
    )
}

export default ProfileButton