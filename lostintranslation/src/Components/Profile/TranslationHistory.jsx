import { STORAGE_KEY_USER } from "../../Const/storageKey";
import { useUser } from "../../Context/UserContext";
import {
  readStorage,
  removeStorageHistory,
} from "../../Utils/Storage";
import { clearApiHistory } from "../../Api/user";
import { useEffect } from "react";


const TranslationHistory = () => {

  const { user, setUser } = useUser(readStorage(STORAGE_KEY_USER))

  const handleClearHistory = () => {
    setUser(removeStorageHistory(STORAGE_KEY_USER))
    clearApiHistory(user);
  };

  const createHistoryList = () => {
    const translationsList = []
    console.log(user)
    console.log(user.translations)
    user.translations.map(key => {
      translationsList.push(<span>{key}</span>)
      translationsList.push(<br />)
    })
    return translationsList
  }

  useEffect(() => {

    createHistoryList()
  }, [user]);

  return (
    <>
      <div id="history_text">
        {createHistoryList()}
      </div>
      <button id="clearHistory" onClick={handleClearHistory}>Clear history</button>


    </>
  );
};
export default TranslationHistory;
