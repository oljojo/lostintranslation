import { STORAGE_KEY_USER } from "../../Const/storageKey"
import { useUser } from "../../Context/UserContext"
import { storageDelete } from "../../Utils/Storage"

export const ProfileHeader = ({ logout }) => {
    const { user, setUser } = useUser()
    const handeleLogoutClick = () => {
        if (window.confirm("are you sure ?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <>

            <button id="profileHeder" onClick={handeleLogoutClick}>Logout</button>



        </>
    )
}
export default ProfileHeader