import { useState } from "react";
import { useEffect } from "react";
import { useTranslation } from "../../Context/TranslationContext";
import { useUser } from "../../Context/UserContext";
import { addApiHistory } from "../../Api/user";
import { updateStorageHistory } from "../../Utils/Storage";
import { STORAGE_KEY_USER } from "../../Const/storageKey";
import ProfileHeader from "../Profile/ProfileHeader";

const TranslationResult = () => {
  const [translationImg, setTranslationImg] = useState(null);
  const { translation } = useTranslation();
  const { user, setUser } = useUser();

  useEffect(() => {
    if (translation !== null) {
      const charArray = translation.split("");
      const imageArray = [];
      charArray.map((key, index) => {
        if (key.match(/[a-zA-Z]/)) {
          imageArray.push(
            <img src={"img/" + key + ".png"} alt={key + index} key={index} />
          );
        } else {
          imageArray.push(<span>&emsp;&emsp;</span>);
        }
      });
      setTranslationImg(imageArray);
      setUser(updateStorageHistory(STORAGE_KEY_USER, translation))
      addApiHistory(user, translation)
    }
  }, [translation]);

  return (
    <>
      <div id="Translation_form">
        <h4>Translation:</h4>
        {translationImg}

       
      </div>
      
    </>
  );
};
export default TranslationResult;
