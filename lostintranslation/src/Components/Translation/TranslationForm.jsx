import { useForm } from "react-hook-form"
import { useTranslation } from "../../Context/TranslationContext"






const TranslationForm = () => {

    const { register, handleSubmit, formState: { errors } } = useForm()
    const { translation, setTransation } = useTranslation()

    const handleOnSubmit = ({ translationText }) => {
        setTransation(translationText)
    }

    return (
        <>
            <form id="translation_form" onSubmit={handleSubmit(handleOnSubmit)}>
            <h2 id="lostInTransText">Lost in translation</h2>
                <div id="translation_input">
                    
                    <input type="text" placeholder="Translation text" {...register("translationText")} />
                </div>
            </form>

        </>
    )
}

export default TranslationForm