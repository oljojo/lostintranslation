
import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from 'react-router-dom'
import { useUser } from "../../Context/UserContext"
import { loginUser } from "../../Api/user"
import {STORAGE_KEY_USER} from "../../Const/storageKey"
import { saveStorage } from "../../Utils/Storage"


const usernameConfig = {
  required: true,
  minLength: 2,
};

const LoginForm = () => {
  const { register, handleSubmit, formState: { errors } } = useForm()
  const { user, setUser } = useUser()
  const navigate = useNavigate()


  const [ loading, setLoading ] = useState(false)
  const [ apiError, setApiError ] = useState(null)

  useEffect(() => {
    if(user !== null) {
      navigate('translation')
    }
  }, [user, navigate])

  console.log(" User is "+ user)

  const handleOnSubmit = async ({username}) => {
    setLoading(true)
    const [error, userResponse] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if(userResponse !== null) {
      saveStorage(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
    setLoading(false)
  };

  const errorMessage = (() => {
    if (!errors.username){
        return null
    }
    if(errors.username.type === 'required'){
        return <span>Username is required</span>
    }
    if(errors.username.type === 'minLength'){
        return <span>Username is too short</span>
    }
  })()

  return (
    <>
      <form onSubmit={handleSubmit(handleOnSubmit)}>
        <fieldset>
          <label htmlFor="username"></label>
          <input
            type="text"
            placeholder="Enter Username..."
            {...register("username", usernameConfig)}
          />
          {errorMessage}
        </fieldset>
       
        {loading && <p>Logging in...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
    </>
  );
};
export default LoginForm;
